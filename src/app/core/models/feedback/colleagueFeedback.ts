export interface colleagueFeedback{
    feedbackId: number;
    author: string;
    position: string;
    colleagueFeedback: string;
}