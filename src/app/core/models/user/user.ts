import {Project} from './project';

export interface User {
  userID: number;
  firstName: string;
  lastName: string;
  projects: Project[];
  coachees: User[];
  position: string;
  startDate: string;
  birthDate: string;
  orangeGridPath: string;
  email: string;
  roles: string[];
  fullname: string;
}
