import {User} from './user';

export interface Project {
  projectId: number;
  projectName: string;
  companyName: string;
  user: User;
}
