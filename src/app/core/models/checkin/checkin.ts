export interface Checkin{
    date: Date,
    title: string,
    projectRole: string,
    conflict: string,
    stack: string,
    realisation: string,
    expectation: string,
    concern: string,
    goal: string,
    coachFeedback: string,
    userId: number
}