import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import "rxjs-compat/add/operator/do";
import { MsAdalAngular6Service } from "microsoft-adal-angular6";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(
    public router: Router,
    private adalService: MsAdalAngular6Service
  ) {}
  jwt = sessionStorage.getItem('adal.idtoken');

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    this.adalService
      .acquireToken("https://graph.microsoft.com")
      .subscribe((resToken: string) => {
        this.jwt = resToken;
        //console.log("jwt initialized in header");
        //console.log(this.jwt);
      });

    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.jwt}`
      }
    });
    return next.handle(request);
  }
}
