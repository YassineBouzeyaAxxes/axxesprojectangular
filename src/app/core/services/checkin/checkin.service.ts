import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Checkin } from '../../models/checkin/checkin';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CheckinService {
  headers = new HttpHeaders();
  //TODO Change this to be the id of the currently logged in user
  userId: number = 1;

  constructor(private http: HttpClient) {
    this.headers.append('Content-Type', 'application/json');
  }
  getUserCheckins() {
    return this.http.get<Checkin[]>(environment.baseUrl + '/api/checkins/' + this.userId);
  }

  getUserCheckinsByUserId(userId) {
    return this.http.get<Checkin[]>(environment.baseUrl + '/api/checkins/' + userId);
  }

  getDetailById(id: string): Observable<Checkin> {
    return this.http.get<Checkin>(environment.baseUrl + '/api/checkin/' + id);
  }

  postCheckin(checkin: Checkin): Observable<Checkin> {
    return this.http.post<Checkin>(environment.baseUrl + '/api/checkin/' + this.userId, checkin);
  }
}
