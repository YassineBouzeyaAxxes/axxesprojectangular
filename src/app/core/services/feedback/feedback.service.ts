import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { colleagueFeedback } from '../../models/feedback/colleagueFeedback';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { CoachFeedback } from '../../models/feedback/coachFeedback';
import { Checkin } from '../../models/checkin/checkin';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  headers = new HttpHeaders();
  
  constructor(private httpClient: HttpClient) { 
  }

  //TODO Make this userId the current logged in user
  getColleagueFeedback(): Observable<colleagueFeedback>{
    let userId = 1;
    return this.httpClient.get<colleagueFeedback>(environment.baseUrl + '/api/feedback/colleague/' + userId);
  }

  postColleagueFeedback(feedback: colleagueFeedback, userId: number): Observable<colleagueFeedback>{
    return this.httpClient.post<colleagueFeedback>(environment.baseUrl + "/api/feedback/colleague/" + userId, feedback);
  }

  putCoachFeedback(checkinId: number, coachFeedback: CoachFeedback): Observable<Checkin>{
    return this.httpClient.put<Checkin>(environment.baseUrl + "/api/checkin/addCoachFeedback/" + checkinId, coachFeedback);
  }
}
