import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "../../models/user/user";
import { environment } from "../../../../environments/environment";
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: "root"
})
export class UserService {
  headers = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.headers.append("Content-Type", "application/json");
  }

  getUserById(userId: number): Observable<any> {
    return this.http.get<User>(environment.baseUrl + "/api/users/" + userId);
  }

  getAllUsers(): Observable<any[]> {
    return this.http.get<User[]>(environment.baseUrl + "/api/users/");
  }

  getCurrentUser(email: string): Observable<User> {
    return this.http.get<User>(
      environment.baseUrl + "/api/users/email/" + email
    );
  }

  getSessionUser(): any {
    // sessionStorage.getItem('adal.idtoken');
   const userToken =  this.getDecodedAccessToken(
      sessionStorage.getItem("adal.idtoken")
    );

   return userToken;

  }

  getDecodedAccessToken(token: string): any {
    return jwt_decode(token);
  }

  // DOES NOT RETURN COACHEE ARRAY
  getAllCoaches(): Observable<User[]> {
    return this.http.get<User[]>(environment.baseUrl + "/api/users/coaches/");
  }

  assignCoachee(coachId: number, coacheeId: number): Observable<User> {
    return this.http.post<User>(
      environment.baseUrl + "/api/users/" + coachId + "/" + coacheeId,
      null
    );
  }

  deleteCoachee(coachId: number, coacheeId: number): Observable<User> {
    return this.http.delete<User>(
      environment.baseUrl + "/api/users/" + coachId + "/" + coacheeId
    );
  }
}
