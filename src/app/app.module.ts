import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomeComponent } from "./ui/home/home.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import {
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatTabsModule,
  MatToolbarModule,
  MatFormFieldModule,
  MatOptionModule,
  MatSelectModule,
  MatDialogModule,
  MatSnackBarModule,
  MatIconModule
} from "@angular/material";
import { NavbarComponent } from "./ui/navbar/navbar.component";
import { RouterModule, Routes } from "@angular/router";
//import {AuthenticationGuard} from './core/guards/authentication.guard';
import { ProfileComponent } from "./ui/profile/profile.component";
import { CheckinComponent } from "./ui/checkin/checkin.component";
import { RecentCommentsComponent } from "./ui/home/recent-comments/recent-comments.component";
import { BannerButtonsComponent } from "./ui/home/banner-buttons/banner-buttons.component";
import { MatInputModule } from "@angular/material";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ProfileOverviewComponent } from "./ui/profile/profileOverview/profile-overview/profile-overview.component";
import { ProfileItemComponent } from "./ui/profile/profileOverview/profile-overview/profile-item/profile-item.component";
import { MsAdalAngular6Module } from "microsoft-adal-angular6";
import { AuthenticationGuard } from "microsoft-adal-angular6";
import { UserService } from "./core/services/user/user.service";
import { TokenInterceptor } from "./core/interceptors/token.interceptor";
import { ColleagueFeedbackModalComponent } from "./ui/home/colleague-feedback-modal/colleague-feedback-modal.component";
import { RecentCheckinsComponent } from "./ui/home/recent-checkins/recent-checkins.component";
import { EllipsisPipe } from "./core/helpers/ellipsis.pipe";
import { CheckinDetailComponent } from "./ui/checkin/checkin-detail/checkin-detail.component";
import { SearchPipe } from "./core/helpers/search.pipe";
import { AssignCoacheesComponent } from "./ui/admin/assign-coachees/assign-coachees.component";
import { ViewCoacheesComponent } from "./ui/admin/view-coachees/view-coachees.component";
import { CheckinFeedbackOverviewComponent } from "./ui/coach/checkin-feedback-overview/checkin-feedback-overview.component";
import { CheckinFeedbackItemComponent } from "./ui/coach/checkin-feedback-item/checkin-feedback-item.component";
import { CoacheeOverviewComponent } from "./ui/coach/coachee-overview/coachee-overview.component";

const appRoutes: Routes = [
  {path: 'home', component: HomeComponent, canActivate: [AuthenticationGuard]},
  // {path: '', component: HomeComponent},
  {path: 'profile', component: ProfileComponent},
  {path: 'admin', component: ProfileOverviewComponent},
  {path: 'admin/assignCoachees/:coachId', component: AssignCoacheesComponent},
  {path: 'admin/viewCoachees/:coachId', component: ViewCoacheesComponent},
  {path: 'checkinDetail/:checkinId', component: CheckinDetailComponent},
  {path: 'checkin', component: CheckinComponent},
  {path: 'coach/coachee/:coacheeId', component: CheckinFeedbackOverviewComponent},
  {path: 'coach/checkin/:checkinId', component: CheckinFeedbackItemComponent},
  {path: 'coach', component: CoacheeOverviewComponent},

  {
    path: "",
    component: AppComponent,
    pathMatch: "full",
    canActivate: [AuthenticationGuard]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    ProfileComponent,
    CheckinComponent,
    RecentCommentsComponent,
    BannerButtonsComponent,
    ProfileOverviewComponent,

    ProfileItemComponent,
    ColleagueFeedbackModalComponent,
    RecentCheckinsComponent,
    EllipsisPipe,
    CheckinDetailComponent,
    SearchPipe,
    AssignCoacheesComponent,
    ViewCoacheesComponent,
    CheckinFeedbackOverviewComponent,
    CheckinFeedbackItemComponent,
    CoacheeOverviewComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes, { enableTracing: true }),
    MsAdalAngular6Module.forRoot(getAdalConfig),
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatFormFieldModule,
    MatOptionModule,
    MatSelectModule,
    FormsModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatIconModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    HttpClientModule
  ],
  entryComponents: [ColleagueFeedbackModalComponent],
  providers: [
    AuthenticationGuard,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],

  bootstrap: [AppComponent]
})
export class AppModule {}

export function getAdalConfig() {
  return {
    tenant: "edd1c3b6-be87-43bb-92d4-7a911c5cee17",
    clientId: "a08537d8-9c03-42c4-aa0f-649ff6741059",
    redirectUri: "http://localhost:4200",
    endpoints: {
      "https://localhost/Api/": "xxx-bae6-4760-b434-xxx"
    },
    navigateToLoginRequestUrl: false
    //cacheLocation: "sessionStorage"
  };
}

export function getUser(){
  this.userService.getUser("Alex.Rosier@axxes.com")
}
