import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/core/services/user/user.service";
import { User } from "src/app/core/models/user/user";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})
export class ProfileComponent implements OnInit {
  user: User;
  //userService: UserService;

  constructor(private userService: UserService) {
    this.userService = userService;
  }

  ngOnInit() {
    this.getUserMethod(1);
  }

  //TODO Change this in service to logged in user, no arg method
  getUserMethod(id: number) {
    let userGet = this.userService.getUserById(id);
    console.log(this.userService.getCurrentUser);

    return userGet.subscribe(data => {
      this.user = data;
      console.log(data);
    });

  }
}
