import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/core/models/user/user';

@Component({
  selector: 'app-profile-item',
  templateUrl: './profile-item.component.html',
  styleUrls: ['./profile-item.component.css']
})
export class ProfileItemComponent implements OnInit {
  @Input() user: User;

  constructor() { }

  ngOnInit() {
  }

}
