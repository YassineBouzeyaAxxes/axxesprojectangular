import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { UserService } from "src/app/core/services/user/user.service";
import { User } from "src/app/core/models/user/user";

@Component({
  selector: 'app-profile-overview',
  templateUrl: './profile-overview.component.html',
  styleUrls: ['./profile-overview.component.css']
})
export class ProfileOverviewComponent implements OnInit {
  userListList: User[];
  public filter: string;

  constructor(private userService: UserService) {}

  ngOnInit() {
    this.getUserListMethod();
  }
  getUserListMethod() {
    let userGet = this.userService.getAllUsers();

    return userGet.subscribe(data => {
      this.userListList = data;
    });
  }
}
