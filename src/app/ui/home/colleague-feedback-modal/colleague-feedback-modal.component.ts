import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { FeedbackService } from 'src/app/core/services/feedback/feedback.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/core/services/user/user.service';
import { User } from "src/app/core/models/user/user";

@Component({
  selector: 'app-colleague-feedback-modal',
  templateUrl: './colleague-feedback-modal.component.html',
  styleUrls: ['./colleague-feedback-modal.component.css']
})
export class ColleagueFeedbackModalComponent implements OnInit {
  userList: User[];
  constructor(
    private dialogRef: MatDialogRef<ColleagueFeedbackModalComponent>,
    private feedbackService: FeedbackService,
    private userService: UserService,
    private _snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.userService.getAllUsers().subscribe(data => this.userList = data);
  }

  addColleagueFeedbackForm = new FormGroup({
    'userId': new FormControl('', Validators.required),
    'author': new FormControl('',Validators.required),
    'colleagueFeedback': new FormControl('',Validators.required),
    'position': new FormControl('',Validators.required)
  })

  closeColleagueFeedback() {
    this.dialogRef.close();
  }

  submitFeedback(){
    this.feedbackService.postColleagueFeedback(this.addColleagueFeedbackForm.value, this.addColleagueFeedbackForm.get('userId').value).subscribe();
    this.dialogRef.close();
    this.openSnackBar();
  }

  openSnackBar() {
    this._snackBar.open("Added Feedback!", "Close", {
      duration: 2000,
    });
  }
}
