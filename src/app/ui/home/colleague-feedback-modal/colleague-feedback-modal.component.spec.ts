import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ColleagueFeedbackModalComponent } from './colleague-feedback-modal.component';

describe('ColleagueFeedbackModalComponent', () => {
  let component: ColleagueFeedbackModalComponent;
  let fixture: ComponentFixture<ColleagueFeedbackModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ColleagueFeedbackModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColleagueFeedbackModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
