import { Component, OnInit } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { ColleagueFeedbackModalComponent } from "./colleague-feedback-modal/colleague-feedback-modal.component";
import { UserService } from "src/app/core/services/user/user.service";
import { User } from "src/app/core/models/user/user";
import * as jwt_decode from "jwt-decode";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  user : User;
  jwt = sessionStorage.getItem("adal.idtoken");
  constructor(private dialog: MatDialog, private userservice: UserService) {}

  ngOnInit() {
    this.initializeUser();
  }

  addColleagueFeebackClick() {
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.minWidth = "600px";

    this.dialog.open(ColleagueFeedbackModalComponent, dialogConfig);
  }
  getDecodedAccessToken(token: string): any {
    return jwt_decode(token);
  }

  initializeUser() {
    const userToken = this.userservice.getSessionUser();
    this.userservice.getCurrentUser(userToken.upn).subscribe(data => {
      //this.user = data;
      localStorage.setItem('currentUser', JSON.stringify(data));
    });
    //console.log(sessionStorage.getItem("adal.idtoken").upn);
  }
}
