import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentCheckinsComponent } from './recent-checkins.component';

describe('RecentCheckinsComponent', () => {
  let component: RecentCheckinsComponent;
  let fixture: ComponentFixture<RecentCheckinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentCheckinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentCheckinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
