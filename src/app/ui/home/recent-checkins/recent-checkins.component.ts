import { Component, OnInit } from '@angular/core';
import { CheckinService } from 'src/app/core/services/checkin/checkin.service';

@Component({
  selector: 'app-recent-checkins',
  templateUrl: './recent-checkins.component.html',
  styleUrls: ['./recent-checkins.component.css']
})
export class RecentCheckinsComponent implements OnInit {
  checkins;

  constructor(private checkinService: CheckinService) { }

  ngOnInit() {
    this.checkinService.getUserCheckins().subscribe(data => this.checkins = data);
  }

}
