import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BannerButtonsComponent } from './banner-buttons.component';

describe('BannerButtonsComponent', () => {
  let component: BannerButtonsComponent;
  let fixture: ComponentFixture<BannerButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BannerButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
