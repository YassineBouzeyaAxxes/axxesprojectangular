import { Component, OnInit } from '@angular/core';
import { FeedbackService } from '../../../core/services/feedback/feedback.service';

@Component({
  selector: 'app-recent-comments',
  templateUrl: './recent-comments.component.html',
  styleUrls: ['./recent-comments.component.css']
})
export class RecentCommentsComponent implements OnInit {
  array;
  someVar;

  constructor(private feedbackService: FeedbackService) { }

  ngOnInit() {
    this.feedbackService.getColleagueFeedback().subscribe((data) => {
      console.log(data);
      this.array = data;
      this.someVar = data[0].author;
    });
  }

}
