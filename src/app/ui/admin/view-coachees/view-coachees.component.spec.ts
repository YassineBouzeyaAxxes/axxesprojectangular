import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCoacheesComponent } from './view-coachees.component';

describe('ViewCoacheesComponent', () => {
  let component: ViewCoacheesComponent;
  let fixture: ComponentFixture<ViewCoacheesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewCoacheesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewCoacheesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
