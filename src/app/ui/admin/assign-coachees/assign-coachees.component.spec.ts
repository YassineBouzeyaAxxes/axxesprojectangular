import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignCoacheesComponent } from './assign-coachees.component';

describe('AssignCoacheesComponent', () => {
  let component: AssignCoacheesComponent;
  let fixture: ComponentFixture<AssignCoacheesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignCoacheesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignCoacheesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
