import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/core/models/user/user';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-assign-coachees',
  templateUrl: './assign-coachees.component.html',
  styleUrls: ['./assign-coachees.component.css']
})
export class AssignCoacheesComponent implements OnInit {
  coachId:number;
  coach:User;
  userList: User[];
  coacheeIdList: number[] = [];
  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.coachId = parseInt(this.route.snapshot.paramMap.get('coachId'));
    this.getCoach();
    this.getUsers();
  }

  getCoach(){
    this.userService.getUserById(this.coachId).subscribe(data => {this.coach = data; this.fillCoacheeList(data)});
  }

  getUsers(){
    this.userService.getAllUsers().subscribe(data => this.userList = data);
  }

  addCoachee(coacheeId: number){
    this.userService.assignCoachee(this.coachId, coacheeId).subscribe(() => {this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/admin/assignCoachees/'+this.coachId]);});
  }

  deleteCoachee(coacheeId: number){
    this.userService.deleteCoachee(this.coachId, coacheeId).subscribe(() => {this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/admin/assignCoachees/'+this.coachId]);});
  }

  fillCoacheeList(coach){
    coach.coachees.forEach(user => {
      this.coacheeIdList.push(user.userID);
    });
  }

}
