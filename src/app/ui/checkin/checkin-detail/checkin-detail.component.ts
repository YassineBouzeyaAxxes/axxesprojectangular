import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Checkin } from 'src/app/core/models/checkin/checkin';
import { CheckinService } from 'src/app/core/services/checkin/checkin.service';

@Component({
  selector: 'app-checkin-detail',
  templateUrl: './checkin-detail.component.html',
  styleUrls: ['./checkin-detail.component.css']
})
export class CheckinDetailComponent implements OnInit {
  checkinId: string;
  checkin: Checkin;
  constructor(
    private route: ActivatedRoute,
    private checkinService: CheckinService
  ) {}

  ngOnInit() {
    this.checkinId = this.route.snapshot.paramMap.get('checkinId');
    this.getDetail();
  }

  getDetail(){
    this.checkinService.getDetailById(this.checkinId).subscribe(data => this.checkin = data);
  }

}
