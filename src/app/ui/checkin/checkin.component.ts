import { Component, OnInit } from '@angular/core';
import { CheckinService } from 'src/app/core/services/checkin/checkin.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-checkin',
  templateUrl: './checkin.component.html',
  styleUrls: ['./checkin.component.css']
})
export class CheckinComponent implements OnInit {

  constructor(private checkinService: CheckinService) { }

  addCheckinForm = new FormGroup({
    'title': new FormControl('', Validators.required),
    'projectRole': new FormControl('', Validators.required),
    'conflict': new FormControl('', Validators.required),
    'stack': new FormControl('', Validators.required),
    'realisation': new FormControl('', Validators.required),
    'expectation': new FormControl('', Validators.required),
    'concern': new FormControl('', Validators.required),
    'goal': new FormControl('', Validators.required),
  })

  ngOnInit() {
  }

  submitCheckin(){
    this.checkinService.postCheckin(this.addCheckinForm.value).subscribe();
  }

}
