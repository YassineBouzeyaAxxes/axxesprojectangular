import { Component, OnInit } from '@angular/core';
import { Checkin } from 'src/app/core/models/checkin/checkin';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FeedbackService } from 'src/app/core/services/feedback/feedback.service';
import { CheckinService } from 'src/app/core/services/checkin/checkin.service';
import { User } from 'src/app/core/models/user/user';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-checkin-feedback-item',
  templateUrl: './checkin-feedback-item.component.html',
  styleUrls: ['./checkin-feedback-item.component.css']
})

export class CheckinFeedbackItemComponent implements OnInit {
  checkinId: string;
  checkin: Checkin;
  coachee: User;

  addFeedbackForm = new FormGroup({
    'coachFeedback': new FormControl('', Validators.required),
  })
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private feedbackService: FeedbackService,
    private checkinService: CheckinService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.checkinId = this.route.snapshot.paramMap.get('checkinId');
    this.getDetail();
  }

  getDetail() {
    this.checkinService.getDetailById(this.checkinId).subscribe(data => { this.checkin = data; this.getCoachee(data.userId) });
  }

  getCoachee(userId: number) {
    console.log("getting user: " + userId);
    this.userService.getUserById(userId).subscribe(data => this.coachee = data);
  };

  submitFeedback() {
    this.feedbackService.putCoachFeedback(parseInt(this.checkinId), this.addFeedbackForm.value).subscribe(() => {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['coach/checkin/' + this.checkinId]);
    });
  }

}
