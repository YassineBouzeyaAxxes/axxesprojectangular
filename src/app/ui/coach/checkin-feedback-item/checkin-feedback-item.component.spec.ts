import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckinFeedbackItemComponent } from './checkin-feedback-item.component';

describe('CheckinFeedbackItemComponent', () => {
  let component: CheckinFeedbackItemComponent;
  let fixture: ComponentFixture<CheckinFeedbackItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinFeedbackItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckinFeedbackItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
