import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckinFeedbackOverviewComponent } from './checkin-feedback-overview.component';

describe('CheckinFeedbackOverviewComponent', () => {
  let component: CheckinFeedbackOverviewComponent;
  let fixture: ComponentFixture<CheckinFeedbackOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CheckinFeedbackOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckinFeedbackOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
