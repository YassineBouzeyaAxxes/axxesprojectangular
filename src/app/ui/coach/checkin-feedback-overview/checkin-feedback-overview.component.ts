import { Component, OnInit } from '@angular/core';
import { CheckinService } from 'src/app/core/services/checkin/checkin.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-checkin-feedback-overview',
  templateUrl: './checkin-feedback-overview.component.html',
  styleUrls: ['./checkin-feedback-overview.component.css']
})
export class CheckinFeedbackOverviewComponent implements OnInit {
  checkins;
  coacheeId
  constructor(private checkinService: CheckinService,
    private route: ActivatedRoute,
    ) { }

  ngOnInit() {
    this.coacheeId = parseInt(this.route.snapshot.paramMap.get('coacheeId'));
    this.checkinService.getUserCheckinsByUserId(this.coacheeId).subscribe(data => this.checkins = data);
  }
}
