import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoacheeOverviewComponent } from './coachee-overview.component';

describe('CoacheeOverviewComponent', () => {
  let component: CoacheeOverviewComponent;
  let fixture: ComponentFixture<CoacheeOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoacheeOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoacheeOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
