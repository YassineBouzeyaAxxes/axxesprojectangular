import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/models/user/user';
import { UserService } from 'src/app/core/services/user/user.service';

@Component({
  selector: 'app-coachee-overview',
  templateUrl: './coachee-overview.component.html',
  styleUrls: ['./coachee-overview.component.css']
})
export class CoacheeOverviewComponent implements OnInit {
  coachId:number = 1; //TODO this.loggedInUser.userID
  coach:User;
  coacheeList: number[] = [];
  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.getCoach();
  }

  getCoach(){
    // TODO this.loggedInUser.coachees
    this.userService.getUserById(this.coachId).subscribe(data => {this.coach = data; this.fillCoacheeList(data)});
  }

  fillCoacheeList(coach){
    coach.coachees.forEach(user => {
      this.coacheeList.push(user);
    });
    console.log(this.coacheeList);
  }
}
